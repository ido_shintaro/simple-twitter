package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;

@WebFilter(urlPatterns = {"/setting","/edit"})
public class LoginFilter implements Filter{
	public void doFilter(ServletRequest request, ServletResponse responce, FilterChain chain) throws IOException,ServletException{

		HttpSession session = ((HttpServletRequest)request).getSession();
		List<String> errorMessages = new ArrayList<String>();
		User user = (User) session.getAttribute("loginUser");

		if(user != null){
			// NULLでなければ、通常どおりの遷移
			chain.doFilter(request, responce);
		} else{
			errorMessages.add("ログインしてください");
			session.setAttribute("errorMessages", errorMessages);
			// Nullならば、ログイン画面へ飛ばす
			((HttpServletResponse)responce).sendRedirect("./login");
			return;
		}
	}
	public void init(FilterConfig config) throws ServletException{}
	public void destroy(){}
}