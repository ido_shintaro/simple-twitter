package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		boolean isShowMessageForm = false;
		User user = (User) request.getSession().getAttribute("loginUser");
		if (user != null) {
			isShowMessageForm = true;
		}
		/*
		 * String型のuser_idの値をrequest.getParameter("user_id")で
		 * JSPから受け取るように設定
		 * MessageServiceのgetMessageに引数としてString型のuser_idを追加
		 */
		String userId = request.getParameter("user_id");
		String startTime = request.getParameter("startTime");
		String endTime = request.getParameter("endTime");
		List<UserMessage> messages = new MessageService().select(userId,startTime,endTime);
		//↓追加
		List<UserComment> comments = new CommentService().select();


		request.setAttribute("messages", messages);
		//↓追加
		request.setAttribute("comments", comments);
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.setAttribute("startTime", startTime);
		request.setAttribute("endTime", endTime);
		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}
}